import timeit


def fib_recursive(number):
    if number in [0, 1]:
        return number
    return fib_recursive(number - 1) + fib_recursive(number - 2)


def fib_buttom_up(number):
    f0 = 0
    f1 = 1
    f2 = 2
    for i in range(2, number + 1):
        f2 = f0 + f1
        f0 = f1
        f1 = f2
    return f2


if __name__ == "__main__":
    print(
        "recursive fibonacci: ",
        fib_recursive(5),
        timeit.timeit("fib_recursive(5)", globals=globals()),
    )
    print(
        "buttom up fibonacci: ",
        fib_buttom_up(5),
        timeit.timeit("fib_buttom_up(5)", globals=globals()),
    )
