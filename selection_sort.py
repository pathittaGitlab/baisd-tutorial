def SelectionSort(data):
    sorted_data = data.copy()
    for i in range(len(sorted_data) - 1, 0, -1):
        maxI = 0
        for j in range(1, i + 1):
            if sorted_data[j] > sorted_data[maxI]:
                maxI = j
        sorted_data[i], sorted_data[maxI] = sorted_data[maxI], sorted_data[i]
    return sorted_data


if __name__ == "__main__":
    data = list(map(int, input("Enter 5 number: ").split()))

    sorted_data = SelectionSort(data)

    print(sorted_data)
